﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Server.Model;
using Server.Model.Abstracts;
using Server.Common;
using Server.Rest;

namespace Product.server.rest.matrixRisks
{
    [Route("model/process/[controller]")]
    public class MatrixRisks : RestBase
    {
        [HttpGet("{process}/{ideaId}")]
        public Dictionary<string, object> Get(int ideaId)
        {
            var ib = new ItemBase(instance, "matrix_risks", currentSession);
            var retval = new Dictionary<string, object>();
            
            var search = new Dictionary<string, object>();
            search.Add("idea_id", ideaId);

            var items = ib.GetItems(search);
            if (items.Count > 0)
            {
                retval = ib.GetItem((int) items[0]["item_id"]);
            }
            return retval;
        }


        [HttpPut("{process}/{ideaId}")]
        public Dictionary<string, object> Put(string process, int ideaId, [FromBody]Dictionary<string, object> data)
        {
            var ib = new ItemBase(instance, "matrix_risks", currentSession);

            if (data.ContainsKey("item_id") && Conv.toInt(data["item_id"]) > 0) {
                return ib.SaveItem(data);
            } else {
                return ib.AddItem(data);
            }
        }

    }
}