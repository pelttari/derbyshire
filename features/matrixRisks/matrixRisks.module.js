﻿(function () {
    'use strict';

    angular
        .module('customer.features')
        .config(MatrixRisksConfig)       
        .controller('MatrixRisksController', MatrixRisksController)
        .service('MatrixRisksService', MatrixRisksService);

    MatrixRisksConfig.$inject = ['ViewsProvider', 'ViewConfiguratorProvider'];
    function MatrixRisksConfig(ViewsProvider, ViewConfiguratorProvider) {
	    ViewConfiguratorProvider.addConfiguration('process.matrixRisks', {
            onSetup: {
                ListProcesses: function (SettingsService, ViewService, $filter) {
                    return SettingsService.ListProcesses().then(function (processes) {
                        return $filter('orderBy')(processes);
                    });
                },
                ItemColumns: function (SettingsService, StateParameters, $filter) {             
                    return SettingsService.ItemColumns(StateParameters.process).then(function (columns) {
                        var itemColumns = {};
                        itemColumns.lists = [];
                        itemColumns.int = [];
                        columns = $filter('orderBy')(columns);
                        angular.forEach(columns, function (key) {
                            if (key.DataType == 1) {
                                itemColumns.int.push(key);
                            }                           
                            if (key.DataType == 6) {
                                itemColumns.lists.push(key);
                            }
                        });
                        return itemColumns;
                    });
                }
            },
			tabs: {
				default: {
                    list: function (resolves) {
                        return {
                            edit: true,
                            type: 'select',
                            selectOptions: resolves.ListProcesses,
                            label: 'Select List 1',
                            selectValue: 'ProcessName',
                            selectName: 'Translation',
                            unselectable: true
                        }
                    },
                    parent_process_column: function (resolves) {
                        return {
                            edit: true,
                            type: 'select',
                            selectOptions: resolves.ItemColumns.lists,
                            label: 'Select Light column',
                            selectValue: 'Name',
                            selectName: 'Translation',
                            unselectable: true
                        }
                    },
                    parent_process_value: function (resolves) {
                        return {
                            edit: true,
                            type: 'select',
                            selectOptions: resolves.ItemColumns.int,
                            label: 'Select selected value column',
                            selectValue: 'Name',
                            selectName: 'Translation',
                            unselectable: true
                        }
                    },                    
                    icon_list: function (resolves) {
                        return {
                            edit: true,
                            type: 'select',
                            selectOptions: resolves.ListProcesses,
                            label: 'Select Icons list',
                            selectValue: 'ProcessName',
                            selectName: 'Translation',
                            unselectable: true
                        }
                    },
                },
            },
            onFinish: function(resolves, params) {

            }
	    });        
        ViewsProvider.addFeature( 'process.matrixRisks', "matrixRisks", "MATRIXRISKS" , ['read', 'write']);

        ViewsProvider.addView('process.matrixRisks', {
            template: 'customer/features/matrixRisks/matrixRisks.html',
            controller: 'MatrixRisksController',
			level: 2,
            resolve: {
                Rows: function (StateParameters, MatrixRisksService) {
                    var StateParams = StateParameters;
                    return MatrixRisksService.getData(StateParams.process, StateParams.itemId).then(function (data) {
                        return data;
                    });
                },                
                List: function (StateParameters, ViewConfigurator, MatrixRisksService) {
                    var StateParams = StateParameters;
                    // var confs = ViewConfigurator.getConfiguration('matrixRisks', StateParams.process);
                    return MatrixRisksService.getParentProcess(StateParams.list).then(function (data) {
                        return data;
                    });
                },
                IconList: function (StateParameters, ViewConfigurator, MatrixRisksService) {
                    var StateParams = StateParameters;
                    // var confs = ViewConfigurator.getConfiguration('matrixRisks', StateParams.process);
                    return MatrixRisksService.getParentProcess(StateParams.icon_list).then(function (data) {
                        return data;
                    });
                },               
            }
        })
    }

    MatrixRisksService.$inject = ['ApiService'];
    function MatrixRisksService(ApiService) {
	    var self = this;
        var Items = ApiService.api('model/process/Items');
        var MatrixRisks = ApiService.api('model/process/MatrixRisks');


        self.getParentProcess = function (process) {
            return Items.get([process]);
        };

        self.getData = function (process, itemId) {
            return MatrixRisks.get([process, itemId]);
        };

        self.saveData = function (process, itemId, data) {
            return MatrixRisks.save([process, itemId], data);
        };        
        
    }

    MatrixRisksController.$inject = ['$q', '$scope', '$translate', 'SaveService', 'StateParameters', 'MatrixRisksService', 'ProcessService', 'Rows', 'Data', 'IconList', 'ViewConfigurator'];
    function MatrixRisksController($q, $scope, $translate, SaveService, StateParameters, MatrixRisksService, ProcessService, Rows, Data, IconList, ViewConfigurator) {

        var confs = StateParameters;

        console.log('Rows', Rows);
        
        $scope.data = Rows;

        $scope.selectValue = function(value) {
            if (angular.isDefined(value)) {
                $scope.data.value = value;
                $scope.data.idea_id = StateParameters.itemId;
                
                MatrixRisksService.saveData(StateParameters.process, StateParameters.itemId, $scope.data).then(function(respond) {
                    if (angular.isUndefined($scope.data.item_id) || $scope.data.item_id == null)
                        $scope.data.item_id = respond.item_id;
                });

                ProcessService.getItem(StateParameters.process, StateParameters.itemId).then(function (respond) {
                    if (value >= 1 && value <= 4) {
                        var item = _.find(IconList, {list_symbol_fill: 'green'});
                        respond[confs.parent_process_column] = [item.item_id];
                    }
                    else if (value >= 5 && value <= 8) {
                        var item = _.find(IconList, {list_symbol_fill: 'yellow'});
                        respond[confs.parent_process_column] = [item.item_id]; 
                    }
                    else if (value >= 9 && value <= 12) {
                        var item = _.find(IconList, {list_symbol_fill: 'orange'});
                        respond[confs.parent_process_column] = [item.item_id]; 
                    }
                    else if (value >= 13 && value <= 16) {
                        var item = _.find(IconList, {list_symbol_fill: 'red'});
                        respond[confs.parent_process_column] = [item.item_id];                  
                    }
                    respond[confs.parent_process_value] = value;
                    
                    ProcessService.setDataChanged(StateParameters.process, StateParameters.itemId);
                });
              
            }
        } 
    }
})();
