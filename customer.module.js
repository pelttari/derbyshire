﻿(function () {
    'use strict';

    angular
        .module('customer', ['customer.features', 'customer.pages', 'customer.dialogs', 'customer.widgets'])
		.config(BackgroundConfig);
		BackgroundConfig.$inject=['BackgroundsProvider'];
		function BackgroundConfig(BackgroundsProvider) {
            BackgroundsProvider.addBackground('derby_banner', 'derby_banner');
            BackgroundsProvider.addBackground('derby_car', 'derby_car');
            BackgroundsProvider.addBackground('derby_p1', 'derby_p1');
            BackgroundsProvider.addBackground('derby_p2', 'derby_p2');            
        }
})();